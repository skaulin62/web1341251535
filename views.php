<!DOCTYPE html>
<html>
<head>
	<title>OnVidGames</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="css/style.css">	
	<link rel="shortcut icon" href="img/logo1.png" type="image/png">
	
<body>
		
	<header>
				<a href="index.php"><img src="img/logo.png" width="200" height="200"></a>
				<ul class="navigation">
					<li><a href="#">НОВОСТИ</a>
						
					</li>
					<li><a href="views.php">ОБЗОРЫ</a></li>
					<li><a href="contacts.php">КОНТАКТЫ</a></li>
				
						
				
				</ul>
				<button class="signin" id="signin">Авторизоваться</button>
				
	</header>
	<div class="back-views">
		<div class="contents-views">
			<h3>ОБЗОРЫ</h3>
			<div class="container-create-view">
				<input placeholder="Начните писать" type="" name="">
				<button>СОЗДАТЬ</button>
			</div>
			<div class="list-category-views">
				<a href="">ВСЕ</a>
				<a href="">CS:GO</a>
				<a href="">DOTA 2</a>
				<a href="">MINECRAFT</a>
			</div>


		</div>
	</div>
	<div id="overlay-modal-signin">
		
		<div class="modal-signin" id="modal-signin">
			<form>
				<h1>Авторизация</h1>
				<div class="inputs-signin">
					<label>Логин</label>
					<input type="" name="">
					<label>Пароль</label>
					<input type="" name="">
				</div>
				<a class="forgot-password" href="">Забыли пароль?</a>
				<button class="input">Войти</button>
				<a class="dont-acc" href="">Нет аккаунта? Зарегистрироваться</a>
			</form>
		</div>
	</div>
	

	
	<footer>
			<div class="content-footer">
				<span class="copyright">Copyright ©</span>
				<div class="nav-footer">
					<a href="">НОВОСТИ</a>
					<a href="">ОБЗОРЫ</a>
				</div>
				<div class="contact-footer">
					<span style="padding-bottom: 30px;">КОНТАКТЫ<br></span>
					<span>OnVidGame@gmail.com<br>+79041766231</span>
				</div>
				<div class="social-network-footer">
					<span>Соц. сети</span>
					<div class="icon-social-network">
							<a target="_block" href="https://discord.com/"><img width="30" src="img/Logo_discord.png"></a>
					<a target="_block" href="https://www.instagram.com/"><img width="30" src="img/instagramm.png"></a>
					<a target="_block" href=""><img width="30" src="img/photo.png"></a>
					</div>
				</div>

			</div>
		</footer>
	<script type="text/javascript" src="signin.js"></script>
</body>
</html>