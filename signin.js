// Get the modal
var modal = document.getElementById('modal-signin');
var overlaymodal = document.getElementById('overlay-modal-signin');
// Get the button that opens the modal
var btn = document.getElementById("signin");



// When the user clicks the button, open the modal 
btn.onclick = function() {
    modal.style.display = "block";
    overlaymodal.style.display = "block";
}



// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
	if (event.target == overlaymodal) {
        modal.style.display = "none";
        overlaymodal.style.display = "none";
    }

    
}